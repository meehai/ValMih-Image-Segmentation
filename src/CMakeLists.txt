cmake_minimum_required(VERSION 2.8)

include_directories(utils)

add_subdirectory(segmentation_algorithms)
add_subdirectory(voting_algorithms)