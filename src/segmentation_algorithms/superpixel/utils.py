import random
import numpy as np

class Singleton(type):
	classInstances = {}
	def __call__(classType, *args, **kwargs):
		if not classType in classType.classInstances:
			classType.classInstances[classType] = super(Singleton, classType).__call__(*args, **kwargs)
		return classType.classInstances[classType]

# calling this twice guarantees to return a different number every time.
class UniqueRandint(metaclass=Singleton):
	def __init__(self):
		self.last = None
		random.seed()

	def __call__(self, left, right):
		assert right - left >= 1
		r = random.randint(left, right)
		while r == self.last:
			r = random.randint(left, right)
		self.last = r
		return r

# usage uniqueRandint(left, right)
uniqueRandint = UniqueRandint()

# Given a set of data and a number, pick n unique items from the data
def randomPick(data, n, return_index=False):
	assert n < len(data)
	picked_index = []
	picked_data = []

	for i in range(n):
		while True:
			value = uniqueRandint(0, len(data) - 1)
			if not value in picked_index:
				picked_index.append(value)
				picked_data.append(data[value])
				break
	if return_index:
		return picked_data, picked_index
	return picked_data

# Given an array, reshape it based on the dimensions parameter which represents how many consecutive dimensions to be
#  squashed together.
# Example: (100, 100, 3), dimensions = (2, 1) => (10000, 3)
def squash_array(data, dimensions):
	newDimensions = []
	assert np.sum(np.array(dimensions)) == len(data.shape)
	index = 0
	for i in range(len(dimensions)):
		value = 1
		for j in range(dimensions[i]):
			value *= data.shape[index]
			index += 1
		newDimensions.append(value)
	return data.reshape(newDimensions)