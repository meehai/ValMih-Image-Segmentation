#pragma once

#include <NumpyArray.hpp>
#include <OS_utils.hpp>


extern "C"
{
	EXPORT int region_growing(struct NumpyArray *, struct NumpyArray *, int, bool, bool);
}
