import numpy as np
from scipy import misc
from random import randint
from mpl_toolkits.mplot3d import Axes3D

# Global figures for all plotting
lib_figures = []

# Method that creates a new figure, adds title (or any other arguments that I might come up with)
# Similar to a parent constructor in OOP, needed for all plottings in current stage.
def plot_base(**kwargs):
	import matplotlib.pyplot as plt
	kwargs["figsize"] = None if not "figsize" in kwargs else kwargs["figsize"]

	# Add a new figure or add the first figure.
	if not "new_figure" in kwargs or kwargs["new_figure"] == True or len(lib_figures) == 0:
		lib_figures.append(plt.figure(figsize=kwargs["figsize"]))

	Projection = None if not "projection" in kwargs else kwargs["projection"]
	if Projection == "2d":
		Projection = "rectilinear"
	# If Axis is a different number, ([0] - no. rows, [1] - no. columns, [2] - index in the grid)
	Axis = (1, 1, 1) if not "axis" in kwargs else kwargs["axis"]
	# Send each parameters as individual parameter
	lib_figures[-1].add_subplot(*Axis, projection=Projection)

	# Disable axis or not (defualt not)
	ShowAxis = "on" if not "show_axis" in kwargs else ("on" if kwargs["show_axis"] is True else "off")
	lib_figures[-1].gca().axis(ShowAxis)

	if "title" in kwargs:
		lib_figures[-1].gca().set_title(kwargs["title"])

	if "xlim" in kwargs:
		lib_figures[-1].gca().set_xlim(kwargs["xlim"])

	if "ylim" in kwargs:
		lib_figures[-1].gca().set_ylim(kwargs["ylim"])

	if "xticks" in kwargs:
		lib_figures[-1].gca().set_xticks(kwargs["xticks"])

	if "yticks" in kwargs:
		lib_figures[-1].gca().set_yticks(kwargs["yticks"])

	if "xlabel" in kwargs:
		lib_figures[-1].gca().set_xlabel(kwargs["xlabel"])

	if "ylabel" in kwargs:
		lib_figures[-1].gca().set_ylabel(kwargs["ylabel"])

def plot_image(data, **kwargs):
	import matplotlib.pyplot as plt
	plot_base(**kwargs)

	cmap = kwargs["cmap"] if "cmap" in kwargs else ("gray" if len(data.shape) == 2 else None) 
	plt.imshow(np.array(misc.toimage(data)), cmap=cmap)

def show_plots(block = True):
	import matplotlib.pyplot as plt
	plt.show(block = block)
