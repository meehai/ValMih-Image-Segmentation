import numpy as np

# @param[in] image Original image
# @param[in] clustered_image Image with unique N clusters index from 0 to N
# @return A new image, where each cluster's intensity is modified with the mean value of the original image
def computeMeanSegmentation(image, clustered_image):
	newImage = np.zeros(image.shape, dtype=image.dtype)
	numClusters = np.max(clustered_image)
	for i in range(numClusters + 1):
		indexes = np.where(clustered_image == i)
		if len(indexes[0]) > 0:
			meanValue = np.mean(image[indexes], axis=0)
			newImage[indexes] = meanValue
	return newImage
