import numpy as np

class NYUDepthReader:
	def __init__(self, datasetPath, randomOrder=False, dataSplit=(60, 30, 10)):
		self.datasetPath = datasetPath
		self.randomOrder = randomOrder
		self.dataSplit = dataSplit
		self.setup()

	# Generic infinite generator, that simply does a while True over the iterate_once method (which only goes one epoch)
	def iterate(self, type, miniBatchSize=None):
		while True:
			for items in self.iterate_once(type, miniBatchSize):
				yield items

	# numData variable dictionary must be created by the setup method for this method to work. 
	def getNumIterations(self, type, miniBatchSize):
		return self.numData[type] // miniBatchSize + (self.numData[type] % miniBatchSize != 0)

	def setup(self):
		import h5py
		assert len(self.dataSplit) == 3 and self.dataSplit[0] >= 0 and self.dataSplit[1] >= 0 \
			and self.dataSplit[2] >= 0 and self.dataSplit[0] + self.dataSplit[1] + self.dataSplit[2] == 100

		self.dataset = h5py.File(self.datasetPath)
		trainStartIndex = 0
		testStartIndex = self.dataSplit[0] * len(self.dataset["images"]) // 100
		validateStartIndex = testStartIndex + (self.dataSplit[1] * len(self.dataset["images"]) // 100)

		self.indexes = {
			"train" : (trainStartIndex, testStartIndex),
			"test" : (testStartIndex, validateStartIndex),
			"validate" : (validateStartIndex, len(self.dataset["images"]))
		}

		self.numData = {
			"train" : testStartIndex,
			"test" : validateStartIndex - testStartIndex,
			"validate" : len(self.dataset["images"]) - validateStartIndex
		}

		if self.randomOrder == True:
			self.orders = {
				"train" : np.random.permutation(self.numData["train"]) + self.indexes["train"][0],
				"test" : np.random.permutation(self.numData["test"]) + self.indexes["test"][0],
				"validate" : np.random.permutation(self.numData["validate"]) + self.indexes["validate"][0],
			}
		else:
			self.orders = None

		print("[NYUDepth Reader] Setup complete.")

	def getIterationImages(self, startIndex, endIndex, which, type):
		if self.randomOrder == False:
			return self.dataset[which][startIndex : endIndex]
		else:
			firstImage = self.dataset[which][self.orders[type][startIndex]]
			images = np.zeros((endIndex - startIndex, *firstImage.shape), dtype=firstImage.dtype)
			images[0] = firstImage
			for i in range(startIndex + 1, endIndex):
				images[i - startIndex] = self.dataset[which][self.orders[type][i]]
			return images

	def iterate_once(self, type, miniBatchSize=None):
		assert type in ("train", "test", "validate")
		if miniBatchSize == None:
			miniBatchSize = self.numData[type]

		for i in range(self.getNumIterations(type, miniBatchSize)):
			startIndex = self.indexes[type][0] + i * miniBatchSize
			endIndex = min(self.indexes[type][0] + (i + 1) * miniBatchSize, self.indexes[type][1])
			# N x 3 x 640 x 480 => N x 480 x 640 x 3
			images = self.getIterationImages(startIndex, endIndex, "images", type)
			images = np.swapaxes(images, 1, 3).astype(np.float32)

			segmentations = self.getIterationImages(startIndex, endIndex, "labels", type)
			segmentations = np.swapaxes(segmentations, 1, 2)

			yield images, segmentations

	# Mean for 640x480x3, for each color channel
	def getMean(self, shape=None):
		# return np.array([123.10519600373294, 101.01146324322507, 90.903207081417833])
		return 105.00662210945862

	# Std for 640x480x3, for each color channel
	def getStd(self, shape=None):
		return 70.95277349081088
		# return np.array([69.84765880279895, 67.36989108803144, 68.8121305956339])