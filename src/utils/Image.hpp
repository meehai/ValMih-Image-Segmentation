/* Image.hpp - Generic class for images and convert operators for various image processing libraries */
#ifndef IMAGE_HPP
#define IMAGE_HPP

#include <cassert>
#include <cstdint>
#include <cstring>
#include <iostream>
#include <type_traits>
#include <tuple>
#include <vector>
#include <stdexcept>
#include <NumpyArray.hpp>

template <typename T=std::uint8_t, bool OwnsMemory=true>
struct Image {
	Image() = delete;
	/* Only allocates the image without setting any value to the allocated memory */
	Image(std::size_t, std::size_t, std::size_t = 1);
	/* Compatibility with NumpyArray */
	Image(const NumpyArray *);
	/* Destructor, free the allocated memory */
	~Image();

	/* Overload for image access (h, w), instead of using getter/setters. */
	const T &operator()(std::size_t, std::size_t, std::size_t = 0) const;
	T &operator()(std::size_t, std::size_t, std::size_t = 0);
	const T &operator()(std::pair<std::size_t, std::size_t>) const;
	T &operator()(std::pair<std::size_t, std::size_t>);
	const T &operator()(std::tuple<std::size_t, std::size_t, std::size_t>) const;
	T &operator()(std::tuple<std::size_t, std::size_t, std::size_t>);

	T *getImage() const;
	std::size_t getWidth() const;
	std::size_t getHeight() const;
	std::size_t getDepth() const;

	void fill(const T& value);


private:
	T *image = nullptr;
	const std::size_t height, width, depth;
};

/* Used for std::cout/cerr printing */
template <typename T, bool OwnsMemory>
std::ostream &operator<<(std::ostream &os, const Image<T, OwnsMemory> &image)
{
	os << "Image (" << image.getHeight() << ", " << image.getWidth() << ", " << image.getDepth() << ")";
	return os;
}

template <typename T, bool OwnsMemory>
inline
Image<T, OwnsMemory>::Image(std::size_t height, std::size_t width, std::size_t depth) : height(height), width(width),
	depth(depth) {
	if(OwnsMemory) {
		this->image = new T[height * width * depth](); // zero initialize
		if (this->image == nullptr)
			throw std::runtime_error("Image is nullptr");
	}
}

template <typename T, bool OwnsMemory>
inline
Image<T, OwnsMemory>::Image(const NumpyArray *npArray) : Image<T, OwnsMemory>(npArray->shape[0], npArray->shape[1],
	npArray->shapeSize == 2 ? 1 : npArray->shape[2]) {
	if( !(npArray->shapeSize == 2) && !(npArray->shapeSize == 3) )
		throw std::runtime_error("Only 2D or 3D images supported");

	if(OwnsMemory) {
		/* Due to the fact that python will recall that npArray's memory, we need to copy it to use it. */
		std::memcpy(this->image, npArray->data, this->height * this->width * this->depth * sizeof(double));
	}
	else {
		/* Important as types may be incompatible and result in errors. */
		size_t npArraySize = npArray->type == NumpyArrayType::NP_UINT8 ? 1 : \
			(npArray->type == NumpyArrayType::NP_INT32 || npArray->type == NumpyArrayType::NP_FLOAT32 ? 4 : 8);
		if(npArraySize != sizeof(T))
			throw std::runtime_error("Invalid type size");
		this->image = (T *)npArray->data;
	}
}

template <typename T, bool OwnsMemory>
inline
Image<T, OwnsMemory>::~Image() {
	if(OwnsMemory) {
		if(this->image != nullptr) {
			delete this->image;
		}
		this->image = nullptr;
	}
}

template <typename T, bool OwnsMemory>
inline
const T &Image<T, OwnsMemory>::operator()(std::size_t i, std::size_t j, std::size_t k) const {
	assert(i < this->height && j < this->width && k < this->depth);
	return this->image[i * this->width * this->depth + j * this->depth + k];
}

template <typename T, bool OwnsMemory>
inline
T &Image<T, OwnsMemory>::operator()(std::size_t i, std::size_t j, std::size_t k) {
	assert(i < this->height && j < this->width);
	return this->image[i * this->width * this->depth + j * this->depth + k];
}

template <typename T, bool OwnsMemory>
inline
const T &Image<T, OwnsMemory>::operator()(std::pair<std::size_t, std::size_t> index) const {
	return this->operator()(std::get<0>(index), std::get<1>(index));
}

template <typename T, bool OwnsMemory>
inline
T &Image<T, OwnsMemory>::operator()(std::pair<std::size_t, std::size_t> index) {
	return this->operator()(std::get<0>(index), std::get<1>(index));
}

template <typename T, bool OwnsMemory>
inline
const T &Image<T, OwnsMemory>::operator()(std::tuple<std::size_t, std::size_t, std::size_t> index) const {
	return this->operator()(std::get<0>(index), std::get<1>(index), std::get<2>(index));
}

template <typename T, bool OwnsMemory>
inline
T &Image<T, OwnsMemory>::operator()(std::tuple<std::size_t, std::size_t, std::size_t> index) {
	return this->operator()(std::get<0>(index), std::get<1>(index), std::get<2>(index));
}

template <typename T, bool OwnsMemory>
inline
std::size_t Image<T, OwnsMemory>::getWidth() const {
	return this->width;
}

template <typename T, bool OwnsMemory>
inline
std::size_t Image<T, OwnsMemory>::getHeight() const {
	return this->height;
}

template <typename T, bool OwnsMemory>
inline
std::size_t Image<T, OwnsMemory>::getDepth() const {
	return this->depth;
}

template <typename T, bool OwnsMemory>
inline
T *Image<T, OwnsMemory>::getImage() const {
	return this->image;
}

template <typename T, bool OwnsMemory>
inline
void Image<T, OwnsMemory>::fill(const T& value) {
	for(std::size_t i = 0; i < this->height * this->width * this->depth; i++)
		this->image[i] = value;
}

#endif /* IMAGE_HPP */