# loader.py Loads all external algorithms (cpp or python) and writes a stub for them to be easily called from main
import os
import sys
import numpy as np
from utils.ffi_utils import ffi, ffi_prepare_numpy

# Python algorithms
from segmentation_algorithms.superpixel.superpixel import superpixel
from voting_algorithms.weights_based.weights_based import weights_based

# Directory where all shared libraryies of each algorithm are expected to be stored after compilation.
libDirectory = ".." + os.sep + "build" + os.sep + "lib"

lib_region_growing = None
lib_mean_shift = None
lib_graph_based = None
lib_region_based = None
ffi.cdef('''
	int region_growing(struct NumpyArray *, struct NumpyArray *, int, bool, bool);
	int mean_shift(struct NumpyArray *, struct NumpyArray *, double, double, int);
	int graph_based(struct NumpyArray *, struct NumpyArray *, int, float);
	int region_based(struct NumpyArray *, struct NumpyArray *, struct NumpyArray *, struct NumpyArray *,
		struct NumpyArray *);
''')

def getLibraryName(baseName):
	if os.name == "nt":
		return libDirectory + os.sep + baseName + ".dll"
	else:
		return libDirectory + os.sep + "lib" + baseName + ".so"

def loadLibraries():
	global lib_region_growing, lib_mean_shift, lib_graph_based, lib_region_based
	lib_region_growing = ffi.dlopen(getLibraryName("region_growing"))
	lib_mean_shift = ffi.dlopen(getLibraryName("mean_shift"))
	lib_graph_based = ffi.dlopen(getLibraryName("graph_based"))
	lib_region_based = ffi.dlopen(getLibraryName("region_based"))

def region_growing_algorithm(image, threshold, randomSeeds, randomColors):
	global lib_region_growing
	if lib_region_growing == None:
		loadLibraries()

	result_image = np.zeros(image.shape, dtype=np.uint8)
	result = lib_region_growing.region_growing(ffi_prepare_numpy(image), ffi_prepare_numpy(result_image),\
		int(threshold), bool(randomSeeds), bool(randomColors))
	return result_image

def mean_shift_algorithm(image, spatial_radius, color_radius, max_level):
	global lib_mean_shift
	if lib_mean_shift == None:
		loadLibraries()

	result_image = np.zeros(image.shape, dtype=np.uint8)
	result = lib_mean_shift.mean_shift(ffi_prepare_numpy(image), ffi_prepare_numpy(result_image), \
		float(spatial_radius), float(color_radius), int(max_level))
	return result_image

def graph_based_algorithm(image, minSize, stdDenominator):
	global lib_graph_based
	if lib_graph_based == None:
		loadLibraries()

	result_image = np.zeros(image.shape, dtype=np.uint8)
	result = lib_graph_based.graph_based(ffi_prepare_numpy(image), ffi_prepare_numpy(result_image), int(minSize), \
		float(stdDenominator))
	return result_image

def superpixel_algorithm(image, clustersCount, returnMeanValues=True):
	result_image = superpixel(image, clustersCount=clustersCount, returnMeanValues=returnMeanValues)
	return result_image

def region_based_voting(superPixelImage, regionGrowingImage, meanShiftImage, graphBasedImage):
	global lib_region_based
	if lib_region_based == None:
		loadLibraries()

	result_image = np.ones(superPixelImage.shape[0 : 2], dtype=np.int32) * -1
	result = lib_region_based.region_based(ffi_prepare_numpy(superPixelImage), ffi_prepare_numpy(regionGrowingImage), \
		ffi_prepare_numpy(meanShiftImage), ffi_prepare_numpy(graphBasedImage), ffi_prepare_numpy(result_image))
	return result_image

def weights_based_voting(superPixelImage, regionGrowingImage, meanShiftImage, graphBasedImage, weights, clustersCount):
	superpixel = lambda image : superpixel_algorithm(image, clustersCount=clustersCount, returnMeanValues=False)[0]
	return weights_based(superPixelImage, regionGrowingImage, meanShiftImage, graphBasedImage, weights, superpixel)
