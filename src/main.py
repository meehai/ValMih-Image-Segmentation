import numpy as np
from loader import superpixel_algorithm, region_growing_algorithm, mean_shift_algorithm, graph_based_algorithm
from loader import region_based_voting, weights_based_voting
from utils.nyudepth_reader import NYUDepthReader
from utils.plotter import plot_image, show_plots
from utils.utils import computeMeanSegmentation
from scipy.misc import toimage
from PIL import Image
from datetime import datetime
from argparse import ArgumentParser

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("type", help="Type of running: segment_image, segment_dataset, run_algorithm, compute_weights \
		(last 2 are used for parameter estimation)")
	parser.add_argument("path", help="path to image or dataset")
	parser.add_argument("--algorithms", default="all")
	parser.add_argument("--batch_size", default=150, type=int)
	parser.add_argument("--num_batches", default=1, type=int)

	# Superpixel
	parser.add_argument("--superpixel_clusters_count", default=5, type=int)

	# Region Growing
	parser.add_argument("--region_growing_threshold", default=25, type=int)
	parser.add_argument("--region_growing_random_seeds", default=1, type=int)

	# Mean Shift
	parser.add_argument("--mean_shift_spatial_radius", default=40, type=float)
	parser.add_argument("--mean_shift_color_radius", default=40, type=float)
	parser.add_argument("--mean_shift_max_level", default=5, type=int)

	# Graph Based
	parser.add_argument("--graph_based_min_size", default=2, type=int)
	parser.add_argument("--graph_based_std_denominator", default=5, type=float)

	# Voting techniques
	parser.add_argument("--region_based_voting", default=1, type=int)
	parser.add_argument("--weights_based_voting", default=1, type=int)
	
	args = parser.parse_args()
	assert args.region_growing_random_seeds in (0, 1)
	args.region_growing_random_seeds = bool(args.region_growing_random_seeds)
	args.region_based_voting = bool(args.region_based_voting)
	args.weights_based_voting = bool(args.weights_based_voting)
	assert args.type in ("segment_image", "segment_dataset", "run_algorithm", "compute_weights")
	assert args.algorithms in ("all", "superpixel", "region_growing", "mean_shift", "graph_based")
	return args

def computeDifference(image1, image2):
	return np.linalg.norm(image1.astype(np.float32) - image2.astype(np.float32))

def segment_image(image, label, args):
	meanLabel = computeMeanSegmentation(image, label) if not label is None else None
	numRows = 2 + args.region_based_voting + args.weights_based_voting

	plot_image(image, axis=(numRows, 4, 1), new_figure=True, show_axis=False, title="Image")
	if not label is None:
		plot_image(label, axis=(numRows, 4, 2), new_figure=False, show_axis=False, title="Label", cmap="tab20")
		plot_image(meanLabel, axis=(numRows, 4, 3), new_figure=False, show_axis=False, title="Mean Label")

	# Call Superpixel algorithm
	now = datetime.now()
	superPixelImage, _ = superpixel_algorithm(image, clustersCount=args.superpixel_clusters_count, \
		returnMeanValues=True)
	diffString = "" if label is None else " Diff: %2.2f" % (computeDifference(superPixelImage, meanLabel))
	print("Superpixel took: %s.%s" % (datetime.now() - now, diffString))

	# Call Region Growing algorithm
	now = datetime.now()
	regionGrowingImage = region_growing_algorithm(image, threshold=args.region_growing_threshold, \
		randomSeeds=args.region_growing_random_seeds, randomColors=False)
	diffString = "" if label is None else " Diff: %2.2f" % (computeDifference(regionGrowingImage, meanLabel))
	print("Region Growing took: %s.%s" % (datetime.now() - now, diffString))

	# Call Mean Shift algorithm
	now = datetime.now()
	meanShiftImage = mean_shift_algorithm(image, spatial_radius=args.mean_shift_spatial_radius, \
		color_radius=args.mean_shift_color_radius, max_level=args.mean_shift_max_level)
	diffString = "" if label is None else " Diff: %2.2f" % (computeDifference(meanShiftImage, meanLabel))
	print("Mean Shift took: %s.%s" % (datetime.now() - now, diffString))

	# Call Graph Based algorithm
	now = datetime.now()
	graphBasedImage = graph_based_algorithm(image, minSize=args.graph_based_min_size, \
		stdDenominator=args.graph_based_std_denominator)
	diffString = "" if label is None else " Diff: %2.2f" % (computeDifference(graphBasedImage, meanLabel))
	print("Graph Based took: %s.%s" % (datetime.now() - now, diffString))

	plot_image(superPixelImage, axis=(numRows, 4, 5), new_figure=False, show_axis=False, title="Superpixel")
	plot_image(regionGrowingImage, axis=(numRows, 4, 6), new_figure=False, show_axis=False, title="Region Growing")
	plot_image(meanShiftImage, axis=(numRows, 4, 7), new_figure=False, show_axis=False, title="Mean Shift")
	plot_image(graphBasedImage, axis=(numRows, 4, 8), new_figure=False, show_axis=False, title="Graph Based")

	# Call Region Based voting algorithm
	if args.region_based_voting:
		currentPlotPosition = 9

		now = datetime.now()
		regionImage = region_based_voting(superPixelImage, regionGrowingImage, meanShiftImage, graphBasedImage)
		finalImage = computeMeanSegmentation(image, regionImage)
		diffString = "" if label is None else " Diff: %2.2f" % (computeDifference(finalImage, meanLabel))
		print("Region Based voting took: %s.%s" % (datetime.now() - now, diffString))

		plot_image(regionImage, axis=(numRows, 4, currentPlotPosition), new_figure=False, show_axis=False, \
			cmap="tab20", title="Region Voting")
		plot_image(finalImage, axis=(numRows, 4, currentPlotPosition + 1), new_figure=False, show_axis=False, \
			title="Mean Region Voting")

	# Call Weights Based voting algorithm
	if args.weights_based_voting:
		currentPlotPosition = 9 + 4 * args.region_based_voting

		# Empirically calculated weights for each segmentation algorithm
		weights = [0.29, 0.30, 0.31, 0.10]

		now = datetime.now()
		weightsImage = weights_based_voting(superPixelImage, regionGrowingImage, meanShiftImage, graphBasedImage, \
			weights=weights, clustersCount=6)
		finalImage = computeMeanSegmentation(image, weightsImage)
		diffString = "" if label is None else " Diff: %2.2f" % (computeDifference(finalImage, meanLabel))
		print("Weights Based voting took: %s.%s" % (datetime.now() - now, diffString))

		plot_image(weightsImage, axis=(numRows, 4, currentPlotPosition), new_figure=False, show_axis=False, \
			cmap="tab20", title="Weights Voting")
		plot_image(finalImage, axis=(numRows, 4, currentPlotPosition + 1), new_figure=False, show_axis=False, \
			title="Mean Weights Voting")
	show_plots()

def run_image(args):
	image = np.array(toimage(Image.open(args.path)), dtype=np.uint8)
	segment_image(image, None, args)

def run_dataset(args):
	reader = NYUDepthReader(args.path, randomOrder=False)

	for i, (images, labels) in enumerate(reader.iterate_once("train", miniBatchSize=args.batch_size)):
		if i == args.num_batches:
			break
		for j in range(args.batch_size):
			# Get the image and label
			image = np.ascontiguousarray(images[j], dtype=np.uint8)
			label = np.array(toimage(labels[j]))
			segment_image(image, label, args)
			print("")

def run_algorithm(args):
	reader = NYUDepthReader(args.path)
	print("Running on algorithm %s." % (args.algorithms))

	if args.algorithms == "superpixel":
		algoFunc = lambda image : superpixel_algorithm(image, clustersCount=args.superpixel_clusters_count, \
			returnMeanValues=True)[0]
		algoParams = "Superpixel. Clusters count %d" % (args.superpixel_clusters_count)
	elif args.algorithms == "region_growing":
		algoFunc = lambda image : region_growing_algorithm(image, threshold=args.region_growing_threshold, \
			randomSeeds=args.region_growing_random_seeds, randomColors=False)
		algoParams = "Region Growing. Threshold %d. Random seeds %d" % (args.region_growing_threshold, \
			args.region_growing_random_seeds)
	elif args.algorithms == "mean_shift":
		algoFunc = lambda image : mean_shift_algorithm(image, spatial_radius=args.mean_shift_spatial_radius, \
			color_radius=args.mean_shift_color_radius, max_level=args.mean_shift_max_level)
		algoParams = "Mean Shift. Spatial Radius %2.2f. Color Radius %2.2f. Max level %d" % \
			(args.mean_shift_spatial_radius, args.mean_shift_color_radius, args.mean_shift_max_level)
	elif args.algorithms == "graph_based":
		algoFunc = lambda image : graph_based_algorithm(image, minSize=args.graph_based_min_size, \
			stdDenominator=args.graph_based_std_denominator)
		algoParams = "Graph Based. Min Size %d. Std denominator %2.2f" % (args.graph_based_min_size, \
			args.graph_based_std_denominator)

	result = 0.0
	for i, (images, labels) in enumerate(reader.iterate_once("train", miniBatchSize=args.batch_size)):
		if i == args.num_batches:
			break
		permutation = np.random.permutation(args.batch_size)
		for j in range(args.batch_size):
			image = np.ascontiguousarray(images[permutation[j]], dtype=np.uint8)
			label = np.array(toimage(labels[permutation[j]]))
			meanLabel = computeMeanSegmentation(image, label)

			resultImage = algoFunc(image)
			resultDiff = np.linalg.norm(resultImage - meanLabel)
			result += resultDiff
	result /= args.batch_size * args.num_batches
	print("%s. Result %2.2f" % (algoParams, result))

def compute_weights(args):
	reader = NYUDepthReader(args.path)
	print("Computing weights for Weights Based voting.")

	superPixelDiffSum = 0
	regionGrowingDiffSum = 0
	meanShiftDiffSum = 0
	graphBasedDiffSum = 0

	for i, (images, labels) in enumerate(reader.iterate_once("train", miniBatchSize=args.batch_size)):
		if i == args.num_batches:
			break
		permutation = np.random.permutation(args.batch_size)
		for j in range(args.batch_size):
			image = np.ascontiguousarray(images[permutation[j]], dtype=np.uint8)
			label = np.array(toimage(labels[permutation[j]]))
			meanLabel = computeMeanSegmentation(image, label)

			superPixelImage, _ = superpixel_algorithm(image, clustersCount=args.superpixel_clusters_count, \
				returnMeanValues=True)
			superPixelDiffSum += np.linalg.norm(superPixelImage - meanLabel)

			regionGrowingImage = region_growing_algorithm(image, threshold=args.region_growing_threshold, \
				randomSeeds=args.region_growing_random_seeds, randomColors=False)
			regionGrowingDiffSum += np.linalg.norm(regionGrowingImage - meanLabel)

			meanShiftImage = mean_shift_algorithm(image, spatial_radius=args.mean_shift_spatial_radius, \
				color_radius=args.mean_shift_color_radius, max_level=args.mean_shift_max_level)
			meanShiftDiffSum += np.linalg.norm(meanShiftImage - meanLabel)

			graphBasedImage = graph_based_algorithm(image, minSize=args.graph_based_min_size, \
				stdDenominator=args.graph_based_std_denominator)
			graphBasedDiffSum += np.linalg.norm(graphBasedImage - meanLabel)

	superPixelDiffSum = 1 / superPixelDiffSum
	regionGrowingDiffSum = 1 / regionGrowingDiffSum
	meanShiftDiffSum = 1 / meanShiftDiffSum
	graphBasedDiffSum = 1 / graphBasedDiffSum
	inverseDiffSum = superPixelDiffSum + regionGrowingDiffSum + meanShiftDiffSum + graphBasedDiffSum

	weightSuperPixel = superPixelDiffSum / inverseDiffSum
	weightRegionGrowing = regionGrowingDiffSum / inverseDiffSum
	weightMeanShift = meanShiftDiffSum / inverseDiffSum
	weightGraphBased = graphBasedDiffSum / inverseDiffSum

	print("Superpixel: %2.2f" % (weightSuperPixel))
	print("Region Growing: %2.2f" % (weightRegionGrowing))
	print("Mean Shift: %2.2f" % (weightMeanShift))
	print("Graph Based: %2.2f" % (weightGraphBased))

def main():
	args = getArgs()

	if args.type == "segment_image":
		run_image(args)
	elif args.type == "segment_dataset":
		run_dataset(args)
	elif args.type == "run_algorithm":
		assert args.algorithms != "all"
		run_algorithm(args)
	elif args.type == "compute_weights":
		assert args.algorithms == "all"
		compute_weights(args)

if __name__ == "__main__":
	main()