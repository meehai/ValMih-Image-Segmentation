def weights_based(superPixelImage, regionGrowingImage, meanShiftImage, graphBasedImage, weights, superCallback):
	votedImage = superPixelImage * weights[0] + regionGrowingImage * weights[1] + \
		meanShiftImage * weights[2] + graphBasedImage * weights[3]
	reclusteredImage = superCallback(votedImage)
	return reclusteredImage
